/*
 * Copyright 2016-2017 adesso AG
 * Autor: Markus Krebs, adesso AG, krebs@adesso.de
 * Licensed under the EUPL, Version 1.1 or - as soon they will be approved by the
 * European Commission - subsequent versions of the EUPL (the "Licence"); You may
 * not use this work except in compliance with the Licence.
 *
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed
 * under the Licence is distributed on an "AS IS" basis, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, either express or implied. See the Licence for the
 * specific language governing permissions and limitations under the Licence.
 */

/**
 * @author: Markus Krebs, adesso AG, krebs@adesso.de
 * This file contains encoding and transport functionality for FIDELIO
 *
 */
/*
 * define URLs for FIDELIO eService and eID-Client
 */
var TR03124_URL = "http://127.0.0.1:24727/eID-Client?tcTokenURL=";
var FIDELIO_URL = "https://id1.vx4.eu/fidelio_dev/?data=";
var nav = null;

/*
 * define Navigator depending on the specific Browser
 */
if(!window) {
    var {viewFor} = require("sdk/view/core");
    var window = viewFor(require("sdk/windows").browserWindows[0]);
    nav = window.navigator;
} else {
    nav = navigator;
}
/*
 * define variables and handle Browser specific differences
 */
var nVer = nav.appVersion;
var nAgt = nav.userAgent;
var browserName  = nav.appName;
var fullVersion  = ''+parseFloat(nav.appVersion);
var majorVersion = parseInt(nav.appVersion,10);
var nameOffset, verOffset, ix;

/*
 * Opera specific handling of the version.
 * In Opera 15+, the true version is after "OPR/"
 */
if ((verOffset=nAgt.indexOf("OPR/"))!=-1) {
    browserName = "Opera";
    fullVersion = nAgt.substring(verOffset+4);
}

/*
 * In older Opera, the true version is after "Opera" or after "Version"
 */
else if ((verOffset=nAgt.indexOf("Opera"))!=-1) {
    browserName = "Opera";
    fullVersion = nAgt.substring(verOffset+6);
    if ((verOffset=nAgt.indexOf("Version"))!=-1)
        fullVersion = nAgt.substring(verOffset+8);
}

/*
 * Microsoft InternetExplorer specific handling of the version.
 * In MSIE, the true version is after "MSIE" in userAgent
 */
else if ((verOffset=nAgt.indexOf("MSIE"))!=-1) {
    browserName = "Microsoft Internet Explorer";
    fullVersion = nAgt.substring(verOffset+5);
}

/*
 * Google Chrome specific handling of the version.
 * In Chrome, the true version is after "Chrome"
 */
else if ((verOffset=nAgt.indexOf("Chrome"))!=-1) {
    browserName = "Chrome";
    fullVersion = nAgt.substring(verOffset+7);
}

/*
 * Apple Safari specific handling of the version.
 * In Safari, the true version is after "Safari" or after "Version"
 */
else if ((verOffset=nAgt.indexOf("Safari"))!=-1) {
    browserName = "Safari";
    fullVersion = nAgt.substring(verOffset+7);
    if ((verOffset=nAgt.indexOf("Version"))!=-1)
        fullVersion = nAgt.substring(verOffset+8);
}

/*
 * Mozilla Firefox specific handling of the version.
 * In Firefox, the true version is after "Firefox"
 */
else if ((verOffset=nAgt.indexOf("Firefox"))!=-1) {
    browserName = "Firefox";
    fullVersion = nAgt.substring(verOffset+8);
}

/*
 * Other Browser handling of the version.
 * In most other browsers, "name/version" is at the end of userAgent
 */
else if ((nameOffset=nAgt.lastIndexOf(' ')+1) < (verOffset=nAgt.lastIndexOf('/'))) {
    browserName = nAgt.substring(nameOffset,verOffset);
    fullVersion = nAgt.substring(verOffset+1);
    if (browserName.toLowerCase()===browserName.toUpperCase()) {
        browserName = navigator.appName;
    }
}

/*
 * trimming the fullVersion string at semicolon/space if present
 */
if ((ix=fullVersion.indexOf(";"))!=-1)
    fullVersion=fullVersion.substring(0,ix);
if ((ix=fullVersion.indexOf(" "))!=-1)
    fullVersion=fullVersion.substring(0,ix);

majorVersion = parseInt(''+fullVersion,10);
if (isNaN(majorVersion)) {
    fullVersion  = ''+parseFloat(navigator.appVersion);
    majorVersion = parseInt(navigator.appVersion,10);
}

/*
 * Http-Get-Request-Function
 * Define the xmlhttp-variable as an normal XMLHttpRequest or specific for the InternetExplorer as an ActiveXObject
 */
function httpGet(serviceURL, cb) {
    if (window.XMLHttpRequest) {
        xmlhttp = new XMLHttpRequest();
    } else {
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.responseType = 'arraybuffer';
    xmlhttp.onreadystatechange=function() {
        if (xmlhttp.readyState===4 && xmlhttp.status===200) {
            cb(xmlhttp.response);
        }
    }
    xmlhttp.open("GET", serviceURL, true);
    xmlhttp.send();
}

/*
 * Function that encodes UTF8 to Base64
 * with the help of the BTOA-Method (which encodes a string in base64) and with String-Replacing (Replacing specific signs in the string with others)
 */
function u82b64u(u8) {
    return encodeURIComponent(btoa(String.fromCharCode.apply(null, u8)).replace(/\+/g, '-').replace(/\//g, '_').replace(/=+$/, '').trim());
}

/*
 * Function that decodes Base64 to UTF8
 * with the help of the ATOB-Method (which decodes a base64 encoded string) and also with String-Replacing (Replacing specific signs in the string with others)
 */
function b64u2u8(s) {
    var binString = atob(s.replace(/\-/g, '+').replace(/\_/g, '/').trim());
    var bytes = new Uint8Array(binString.length);
    for (var i = 0; i < binString.length; i++) {
        bytes[i] = binString.charCodeAt(i);
    }
    return bytes;
}

/*
 * "atomic" call to the Service
 */
function callService(cmd, message, cb) {
    var cbor = new Uint8Array(CBOR.encode(message)), ctap = new Uint8Array(1 + cbor.length);
    ctap.set([cmd]);
    ctap.set(cbor, 1);
    var svcMsg = u82b64u(ctap);
    console.log(svcMsg);
    httpGet(TR03124_URL + encodeURIComponent(FIDELIO_URL + svcMsg), function(data) {
        data = new Uint8Array(data, 0, data.length);
        var status = data[0];
        console.log("STATUS: " + status);
        console.log("CTAP-B64: " + btoa(String.fromCharCode.apply(null, data)));
        var msg = CBOR.decode(data.buffer.slice(1));
        console.dir(msg);
        cb(u82b64u(msg['3']), u82b64u(msg['1']), status);
    });
}

/*
 * build Enrollment-variable
 */
var fidelioEN = function fidelioEN(appIdHash, clientDataHash, blackList, cb) {
    blackList = blackList != null ? blackList.map(function(x) { return b64u2u8(x); }) : null;
    console.log(appIdHash);
    console.log(clientDataHash);
    console.log(blackList);
    callService(1, {1 : appIdHash, 2 : b64u2u8(clientDataHash), 5 : blackList}, cb);
};

/*
 * build Authentication-variable
 */
var fidelioAU = function fidelioAU(appIdHash, clientDataHash, whiteList, cb) {
    whiteList = whiteList != null ? whiteList.map(function(x) { return b64u2u8(x); }) : null;
    callService(2, {1 : appIdHash, 2 : b64u2u8(clientDataHash), 3 : whiteList}, cb);
};

/*
 * handle if Browser is Firefox
 */
if(browserName === "Firefox") {
    console.log(browserName);
    var { XMLHttpRequest } = require("sdk/net/xhr");
    exports.fidelioEN = fidelioEN;
    exports.fidelioAU = fidelioAU;
}


